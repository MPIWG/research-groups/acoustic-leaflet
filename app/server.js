var express = require('express')
var app = express()

app.use(express.static('public'))
app.post('/', function (req, res) {
    res.send('POST request to the homepage')
  })

app.listen(3000, () => console.log('Server ready at port 3000'))
