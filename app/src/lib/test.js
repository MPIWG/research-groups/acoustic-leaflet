let markerdata = [{
  type: "text"
}, {
  type: "text"
}, {
  type: "video"
}, {
  type: "text"
}, {
  type: "video"
}, {
  type: "text"
}, {
  type: "video"
}, {
  type: "text"
}, {
  type: "video"
}, {
  type: "text"
}, {
  type: "video"
}, {
  type: "picture"
}, {
  type: "text"
}, {
  type: "picture"
}, {
  type: "video"
}, {
  type: "text"
}, {
  type: "text"
}, {
  type: "text"
}, {
  type: "picture"
}, ]


function getTypeCount(selectedData) {
  let len = selectedData.length;
  let occurances = [];
  while (len--) {
    occurances.push(selectedData[len].type)
  }
  let set = occurances;
  let types = Array.from([...new Set(set)])
  let typelen = types.length;
  while (typelen--) {
    types[typelen] = [types[typelen], 0]
  }
  for (var x = 0; x < occurances.length; x++) {
    for (var y = 0; y < types.length; y++) {
      if (occurances[x] === types[y][0]) {
        types[y][1]++;
      }
    }
  }
  return types;
}


for(let i = 0; i < 100; i++){
  getTypeCount(markerdata);
}

