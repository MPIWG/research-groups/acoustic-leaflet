import { config } from "../config";
import { min, max } from 'd3-array'
import pako from 'pako'

class DataStore {

  constructor () {
    this._data = []
    this._filters = []
    this._selects = []
  }

  addFilter (filter) {
    this._filters.push(filter)
  }

  addSelect (filter) {
    this._selects.push(filter)
  }

  clearFilterForField (field) {
    this._filters = [...this._filters].filter(d => d.field != field)
  }

  clearSelect (field) {
    this._selects = [...this._selects].filter(d => d.field != field)
  }
  loadData (dataUrl, mappings = []) {
    return new Promise((resolve) => {
      fetchData(dataUrl, mappings).then( data => {
        var output = data
        this._data = output
        resolve(data)
      } )
    } )
  }

  get data() {
    return this._data
  }

  get filteredData() {
    if ( ! this._filters.length ) {
      return this._data
    }
    let filteredData = [...this._data]
    for (let i = 0; i < this._filters.length; i++) {
      let filter = this._filters[i]
      filteredData = filter.apply(filteredData)
    }
    return filteredData
  }

  get selectedData() {
    if ( ! this._selects.length ) {
      return []
    }
    let selectedData = this.filteredData
    for (let i = 0; i < this._selects.length; i++) {
      let select = this._selects[i]
      selectedData = select.apply(selectedData)
    }
    return selectedData
  }

  get temporalExtent() {
    if (! this._data.length ) return [new Date(-2000, 0, 1), new Date(2000, 11, 31)]
    return [ config.timeline.startYear ? new Date(config.timeline.startYear, 0, 1) : min(this._data, d => d.date_from),
            config.timeline.endYear ? new Date(config.timeline.endYear, 11, 31) : max(this._data, d => d.date_to) ]
  }
}

function fetchData(dataUrl, mappings) {
  return new Promise( (resolve) => {
    fetch(dataUrl).then( response => {
      return response.json()
    }).then( data  => {
      data = preprocess(data, mappings)
      resolve(data)
    })
  })
}

function preprocess(data, mappings) {
  for (let i = 0; i < mappings.length; i++) {
    let mapping = mappings[i]
    let transformation = mapping.transformation ? mapping.transformation : (d) => d
    data = data.map((d) => {
      if (d[mapping.source]) {
        d[mapping.field] = transformation(d[mapping.source])
      }
      return d
    })
  }
  //data = convertCoordinates(data)
  return data
}

export { DataStore }
