let config = {
  mappings: [
    {
      field: 'id',
      source: 'nid'
    },
    {
      field: 'coordinates',
      source: 'field_coordinates',
      transformation: (d) => {
        let re = /(-?\d*\.\d*)\s(-?\d*\.\d*)/g
        let match = re.exec(d)
        if (match) {
          return [+match[2], +match[1]]
        }
      }
    },
    {
      field: 'place_name',
      source: 'field_address'
    },
    {
      field: 'date_from',
      source: 'field_object_year_start',
      transformation: (d) => {
        return new Date(d, 0, 1)
      }
    },
    {
      field: 'date_to',
      source: 'field_object_year_end',
      transformation: (d) => {
        return new Date(d, 11, 31)
      }
    }
  ],
  url: 'objects.json',
  map: {
    maxTypesInPopup: 3
  },
  timeline: {
    startYear: 1500,
    endYear: false
  },
  graph: {
    color: "rgba(248, 121, 121, .8)",
    color2: "rgba(41, 92, 128, .8)",
    displayLabels: false,
    timeFormat: "YYYY",
    dotSize: 2,
  }
}
export {
  config
}
