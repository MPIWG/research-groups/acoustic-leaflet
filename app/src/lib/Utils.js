
function triggerWindowResize() {
  var el = document;
  var event = document.createEvent('HTMLEvents');
  event.initEvent('resize', true, false);
  el.dispatchEvent(event);
}

export { triggerWindowResize }
