## About

An custom map visualisation based on [LeafletJS](https://leafletjs.com/) for the database at http://acoustics.mpiwg-berlin.mpg.de. It makes use of [VueJS](https://vuejs.org/) and the project has been built using the [Vue CLI](https://cli.vuejs.org/)

The project is led by [Viktoria Tkaczyk](https://www.mpiwg-berlin.mpg.de/en/users/tkaczyk)

The repository is maintained by [Florian Kräutli](https://www.mpiwg-berlin.mpg.de/en/users/fkraeutli)

## How to run

### Option 1: Build and develop locally

Follow the instructions in the Readme file in app/

### Option 2: Use the Docker image

The repository includes a Dockerfile which builds the app into a docker image and serves it using an Nginx web server. You can run the pre-built image, e.g.:

```
$ docker container run -d -p 8888:80 docker.gitlab.gwdg.de/mpiwg/research-groups/acoustic-leaflet
```

and navigating to http://localhost:8888 in your browser.
