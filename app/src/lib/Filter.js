
class Filter {
  constructor(field, callback) {
    this._field = field
    this._callback = callback
  }

  apply(data) {
    return data.filter(this.callback)
  }

  get callback() {
    return this._callback
  }

  set callback(v) {
    this._callback = v
  }

  get field() {
    return this._field
  }

  set field(v) {
    this._field = v
  }

  get value() {
    return this._value
  }

  set value(v) {
    this._value = v
  }
}

class FilterGreaterThan extends Filter {
  constructor(field, lowerBound) {
    super(field, false)
    this.value = lowerBound
  }

  apply(data) {
    return data.filter( d => {
      return d[this.field] && d[this.field] > this.value
    })
  }
}

class FilterLowerThan extends Filter {
  constructor(field, upperBound) {
      super(field, false)
      this.value = upperBound
  }

  apply(data) {
    return data.filter( d => {
      return d[this.field] && d[this.field] < this.value
    })
  }
}

class FilterValues extends Filter {
  constructor(field, values) {
      super(field, true)
      this.value = values
  }

  get value() {
    return this._value
  }

  set value(v) {
    this._value = v.map(d => { return Array.isArray(d) ? d.join() : d })
  }
  

  apply(data) {
    
    return data.filter( d => {
      return d[this.field] && this.value.includes(Array.isArray(d[this.field]) ? d[this.field].join() : d[this.field])
    })
  }
}

export { Filter, FilterGreaterThan, FilterLowerThan, FilterValues }
