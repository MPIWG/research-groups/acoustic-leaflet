FROM nginx
RUN apt-get update && apt-get install -y \
  apt-transport-https \
  curl
RUN curl -sL https://deb.nodesource.com/setup_8.x | bash -
RUN apt-get update && apt-get install -y \
  nodejs
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - && \
  echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt-get update && apt-get install -y yarn
ADD app /app
WORKDIR app
RUN yarn install
RUN yarn run build
RUN mv dist/* /usr/share/nginx/html/
